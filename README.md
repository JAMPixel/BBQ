# BBQ
###### (Jam du 8-9 octobre)
## Theme : Gravité


#### Dev : 
+ Victor Mottet
+ Cyprien Perrier
+ Stiven Aigle

#### Techno utilisé :
+ C++ 14
+ Open GL 3.3
+ glish
+ SDL2 / SDL2_image
+ boost 1.57
+ glm


#### Principe :
Jeu d'enigme basé sur les principes de gravité. Le perso peut gerer la puissance et la direction 
de la gravité pour se déplacer dans tout le niveau. Il peut également générer des bulles de gravité
qui permettent de changer la gravité appliqué à l'intérieur de la ville. 
##### 
Le niveau est constitué de brique immobile qui ne subis pas la gravité et d'autre qui sont soumis à la gravité et qui vont nous aider ou nous empecher de finir le niveau.
##### 
Le but étant d'aller à la fin du niveau.




