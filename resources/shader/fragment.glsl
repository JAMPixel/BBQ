#version 330
out vec4 color;
uniform sampler2D text;
uniform int choix;
uniform float bleurk;
uniform float grave;
uniform float sens;
uniform float rayon;
in vec2 uvOut;
in vec2 realUv;
const float maxGrav = 10;
const float maxRayon = 100;
const float minRayon = 10;
const float pi = 3.14159;

void main() {

    if(choix == 0){
        color = texture(text, uvOut );
    }else if(choix == 1){
        color = vec4(realUv.x, realUv.y, 1., 1. );
    }else if(choix == 2){
        float interpol = (grave+maxGrav) / (maxGrav*2);
        float plop = distance(realUv, vec2(0.5, interpol));
        float angle = atan(realUv.y-0.5, realUv.x-0.5);
        angle = angle/3.14159;

        float tamere = (angle-2+plop);
        color = vec4(tamere, 1-plop, plop, plop);
    }else if(choix == 3){
        float aff = distance(realUv, vec2(0.5,0.5));
        float alpha = 0.5 + (rayon-minRayon)/(maxRayon*2);
        alpha = aff/alpha;
        if(aff < 0.5){
            if(sens < 0){
                color = vec4(1., 0., 0. , alpha);
            }else{
                color = vec4(0., 1., 0., alpha);
            }
        }else{
            color = vec4(0);
        }
    }else if(choix == 4){
        const float ecart = 0.25;
        float y = realUv.y*4;
        float multi = 1.;
        float frac = mod(y, 1) + multi*realUv.x;
        int test = int(y);
        if(test == 3 || test == 1){
            frac = frac +(1-2*multi*realUv.x);

        }
        const float borne = 0.3;
        float alpha = 0.;
        if(frac >=1 && frac <= 1.+borne ){
            alpha = 1.;
        }
        color = vec4( 0.5, 0.5, 0.5, alpha);
    }

}
