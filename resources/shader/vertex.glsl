#version 330
layout (location = 0) in vec3 pos;
layout (location = 1) in vec2 uv;

uniform mat3 scale;
uniform mat3 translate;
uniform vec2 hg;
uniform vec2 bd;
out vec2 uvOut;
out vec2 realUv;
void main() {

    gl_Position = vec4(scale*translate*pos, 1.);
    uvOut = mix(hg, bd, uv);
    realUv = uv;
}
