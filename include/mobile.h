//
// Created by cyprien on 08/10/16.
//

#ifndef BBQ_MOBILE_H
#define BBQ_MOBILE_H


#include <vector>
#include "Case.hpp"
#include "Bulle.hpp"

struct Mobile : public Case{

    Mobile(float posX, float posY,float tailleX, float tailleY,float force = 0);
    float posX ;
    float posY ;
    float force ;
    float tailleX ;
    float tailleY ;
    std::pair<float,float> vitesse;

    virtual void deplacerY(std::vector<Bulle> bul);
    bool collision(std::vector<Mobile*>& murs);
    void collisionmobile(std::vector<Mobile*>& cubes);

};


#endif //BBQ_MOBILE_H
