//
// Created by cyprien on 08/10/16.
//

#ifndef BBQ_PERSO_H
#define BBQ_PERSO_H


#include <vector>
#include "Bulle.hpp"

#include <vector>
#include "mobile.h"
#include "Piston.hpp"

struct Perso {

    float posX ;
    float posY ;
    float elan ;
    float tailleX;
    float tailleY;
    float decaleX = 1/2.0f;
    std::pair<float,float> vitesse;
    Perso(){};
    Perso(float posX, float posY);
    void deplacerX(bool a);
    void deplacerY(std::vector<Bulle> bul) ;
    void sauter(std::vector<Bulle> bul);
    void modgrav(bool b);

    void collision(std::vector<Mobile*>& murs,int &cotecollision, float &distanceinterpenetration);
    void collision(std::vector<Piston *> &murs, int &cotecollision, float &distanceinterpenetration);
};


#endif //BBQ_PERSO_H
