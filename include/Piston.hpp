//
// Created by hole on 09/10/16.
//

#ifndef BBQ_PISTON_HPP
#define BBQ_PISTON_HPP


#include "mobile.h"

struct Piston : public Mobile{
    float xAttache;
    float yAttache;
    float limite;
    int orientation;
    Piston(float posX, float posY, float tailleX, float tailleY, float aX, float aY, float lim, int orientation);

    void deplacerY(std::vector<Bulle> bul) override;


};


#endif //BBQ_PISTON_HPP
