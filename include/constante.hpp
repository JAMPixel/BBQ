//
// Created by joe on 08/10/16.
//

#ifndef BBQ_CONSTANTE_HPP
#define BBQ_CONSTANTE_HPP
namespace constante{
    constexpr int width = 1200;
    constexpr int height = 700;
    extern float gravity;

}
#endif //BBQ_CONSTANTE_HPP
