//
// Created by hole on 08/10/16.
//

#ifndef BBQ_LEVEL_HPP
#define BBQ_LEVEL_HPP

#include <vector>
#include "mobile.h"
#include "perso.h"
#include "Piston.hpp"
#include <Bulle.hpp>


struct Level{
    std::vector< std::vector <int> > base;
    std::vector< std::vector <int> > entity;
    std::vector< Mobile*> murs;
    std::vector< Mobile*> cubes;
    std::vector< Piston*> pist;
    Perso bonhomme;

    int n;
    int m;


    Level (char * fichierLevel, char * fichierEntity);
    void gravitage(std::vector<Bulle> bul);


};


#endif //BBQ_LEVEL_HPP
