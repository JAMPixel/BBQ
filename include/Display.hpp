//
// Created by joe on 08/10/16.
//

#ifndef BBQ_DISPLAY_HPP
#define BBQ_DISPLAY_HPP

#include <glish/Vao.hpp>
#include <glish/Vbo.hpp>
#include <glish/macro.hpp>
#include "perso.h"
#include <glish/Texture.hpp>
#include <iostream>
#include "Case.hpp"
#include <glish/displayText.hpp>
template <typename T>
void affPerso(const Perso &bonhomme, T &prog){
    std::string plop("resources/image/dragon.png");
    static glish::Texture text(plop);
    static float avanceY = 1/2.0f;
    constexpr float decal = 1/4.0f;
    text.bindTexture();
    glish::transformStorage tran;
    tran.scale = glm::vec2(bonhomme.tailleX, bonhomme.tailleY);
    tran.tran = glm::vec2(bonhomme.posX, bonhomme.posY);
    prog.translate = glish::transform(tran, 0.5f, 0.5f);
    prog.hg = glm::vec2(bonhomme.decaleX, avanceY);
    prog.bd = glm::vec2(bonhomme.decaleX+decal, avanceY+decal);
    prog.text = 0;
    prog.choix = 0;
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    //std::cout <<bonhomme.decaleX << std::endl;

}
void changeSens(Perso &perso){
    perso.tailleX *=-1;
}
void majPerso(Perso & perso){
    constexpr  float decal= 1/4.0f;
    perso.decaleX +=decal;
}
template <typename T>
void affCase(const std::vector<Mobile *> & cases, T & prog){
    glish::transformStorage tran;
   for (auto & c : cases){
        tran.scale = glm::vec2(c->tailleX, c->tailleY);
        tran.tran = glm::vec2(c->posX, c->posY);
        prog.translate = glish::transform(tran);
        prog.choix = 1;
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    }
}
template <typename T>
void affPist(const std::vector<Piston *> & cases, T & prog){
    glish::transformStorage tran;
    for (auto & c : cases){
        tran.scale = glm::vec2(c->tailleX, c->tailleY);
        tran.tran = glm::vec2(c->posX, c->posY);
        prog.translate = glish::transform(tran);
        prog.choix = 1;
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    }
    for (auto & c : cases){
        float pY;
        if (c->orientation == 0){
            pY = std::max(c->yAttache,c->posY);
        }
        else {
            pY = std::max(c->yAttache,c->posY)+c->tailleY;
        }
        tran.scale = glm::vec2(c->tailleX*1./3,(std::min(c->yAttache,c->posY) -std::max(c->yAttache,c->posY)));
        tran.tran = glm::vec2(c->posX+c->tailleX*1./3, pY);
        prog.translate = glish::transform(tran);
        prog.choix = 4;
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    }

}

template <typename T>
void affFond(T & prog, int changeFond) {

    static float bl = 0;


    glish::transformStorage fond;
    fond.scale = glm::vec2(constante::width, constante::height);
    prog.translate = glish::transform(fond);
    prog.choix = 2;
    if (!(changeFond % 3)) {
        bl += M_PI / 8;
        prog.bleurk = std::sin(bl);
    }
    prog.grave = constante::gravity;

    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}
template  <typename T>
void affBulle(Bulle & bulle, T& prog) {
    glish::transformStorage tran;
    tran.scale = glm::vec2(bulle.rayon*2);
    tran.tran = glm::vec2(bulle.x, bulle.y);
    prog.choix = 3;
    prog.translate = glish::transform(tran, 0.5, 0.5);
    prog.sens = bulle.gravitance;
    prog.rayon = bulle.rayon;
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}

template <typename T>
void affBulle(std::vector<Bulle> & bulle, T& prog){
    for (auto & b : bulle){
        affBulle(b, prog);
    }
}
#endif //BBQ_DISPLAY_HPP
