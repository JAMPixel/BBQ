//
// Created by hole on 08/10/16.
//

#ifndef BBQ_BULLE_HPP
#define BBQ_BULLE_HPP


struct Bulle {
    float x;
    float y;
    float gravitance;
    float rayon;
    int time;

    Bulle(float x = -100, float y = -100, float gravitance = 0, float rayon = 10, int time = 300);
    void incremente();
    void decremente();
    void reinit (){ x = y = -100; gravitance = 0; rayon = 10; }
    bool deBulle();

};


#endif //BBQ_BULLE_HPP
