//
// Created by joe on 08/10/16.
//
#include <glish/InitOGL.hpp>
#include "constante.hpp"
#include <GL/glew.h>
#include <glish/macro.hpp>
#include <glish/Vao.hpp>
#include <glish/Vbo.hpp>
#include <glish/quad.hpp>
#include "perso.h"

#include <Level.hpp>
#include <Display.hpp>
#include "Bulle.hpp"

int main(int, char**){
    glish::InitOGL win(constante::width, constante::height, "BBQ");
    glish::Vao vao;
    vao.bind();
    glish::Vbo vbo(0, 3, glish::mat::rect);
    glish::Vbo vbouv (1, 2, glish::mat::uv);
    SDL_Event ev;
    const Uint8 * key = SDL_GetKeyboardState(nullptr);

    glish::transformStorage trans;
    GLISH_CREATE_PROGRAM(prog, "resources/shader/vertex.glsl", "resources/shader/fragment.glsl",(scale)(translate)(hg)(bd)(text)(choix)(bleurk)(grave)(sens)(rayon))

    prog.prog.useProgram();
    bool run = true;
    //Perso bonhomme(50.0f, 50.0f); // maintenant c'est dans lvl
    prog.scale = glish::normalizedScreenSpace(constante::width, constante::height);

    Level lvl("./resources/level/lvl1","./resources/level/ent1");
    int sprite = 0;
    float time = SDL_GetTicks();
    std::string lastSens = "droit";
    int changeFond = 0;
    GLISH_DISPLAYTEXT(plop, "resources/shader/vertexText.glsl", "resources/shader/fragmentText.glsl","resources/font/font.png", glm::vec2(constante::width, constante::height),(plop))
    glish::changeColor(plop, glish::color::red);
    constexpr float sizeLetter = 20;
    int puissanceGrav = 0;
    bool appuie = false;
    bool onLeft = true;
    int x, y;
    Bulle bulle(0, 0);

    std::vector<Bulle> bul;


    int cotecollision;
    float distanceinterpenetration;
    std::vector <Mobile *> connard;
    connard.push_back(new Mobile(400,400,20,30));
    int fps = 0;
    while(run){
        time = SDL_GetTicks();
        changeFond++;




        glClear(GL_COLOR_BUFFER_BIT);
        while(SDL_PollEvent(&ev)){
            if(ev.type == SDL_QUIT || ev.key.keysym.sym == SDLK_ESCAPE){
                run = false;
            }else if(ev.key.keysym.sym == SDLK_r){
                constante::gravity = 0.0f;
            }
            if(ev.button.button == SDL_BUTTON_LEFT && ev.type == SDL_MOUSEBUTTONDOWN && !appuie){
                appuie = true;
                onLeft = true;

                SDL_GetMouseState(&x, &y);
                bulle.x = x;
                bulle.y = y;
            }else if(ev.button.button == SDL_BUTTON_LEFT && ev.type == SDL_MOUSEBUTTONUP &&onLeft){
            //}else if(ev.button.button == SDL_BUTTON_LEFT && ev.button.state == SDL_RELEASED && onLeft){
                appuie = false;
                bul.push_back(bulle);
                bulle.reinit();

            }
            if(ev.button.button == SDL_BUTTON_RIGHT && ev.button.state == SDL_PRESSED && !appuie){
                appuie = true;
                onLeft = false;
                SDL_GetMouseState(&x, &y);
                bulle.x = x;
                bulle.y = y;
            }else if(ev.button.button == SDL_BUTTON_RIGHT && ev.button.state == SDL_RELEASED && !onLeft){
                appuie = false;
                bul.push_back(bulle);
                bulle.reinit();

            }

        }
        if(key[SDL_SCANCODE_D] || key[SDL_SCANCODE_RIGHT]){
            lvl.bonhomme.deplacerX(true);
            sprite++;
            if(lastSens == "gauche"){
                lastSens = "droit";
                changeSens(lvl.bonhomme);
            }
            if(!(sprite%10))
                majPerso(lvl.bonhomme);

        }
        if(key[SDL_SCANCODE_A] || key[SDL_SCANCODE_LEFT]){
            lvl.bonhomme.deplacerX(false);
            sprite++;
            if(lastSens == "droit"){
                lastSens = "gauche";
                changeSens(lvl.bonhomme);
            }
            if(!(sprite%10))
                majPerso(lvl.bonhomme);

        }
        if(key[SDL_SCANCODE_SPACE]){
            lvl.bonhomme.sauter(bul);
        }
        if(key[SDL_SCANCODE_LSHIFT] || key[SDL_SCANCODE_UP] || key[SDL_SCANCODE_W]){
            lvl.bonhomme.modgrav(false);
        }
        if(key[SDL_SCANCODE_LCTRL] || key[SDL_SCANCODE_DOWN] || key[SDL_SCANCODE_S]){
            lvl.bonhomme.modgrav(true);
        }
        if(appuie && onLeft) {
            bulle.incremente();
        }
        if(appuie && !onLeft){
            bulle.decremente();
        }

        for(auto &c : lvl.cubes){
            c->collision(lvl.murs);

        }

        lvl.bonhomme.collision(lvl.murs,cotecollision,distanceinterpenetration);

        fps ++;
        lvl.bonhomme.collision(lvl.murs,cotecollision,distanceinterpenetration);
        lvl.bonhomme.collision(lvl.cubes, cotecollision, distanceinterpenetration);
        lvl.bonhomme.collision(lvl.pist, cotecollision, distanceinterpenetration);

        distanceinterpenetration=lvl.bonhomme.posY;
        lvl.bonhomme.vitesse.first = 0;
        affBulle(bulle, prog);
        vao.bind();
        affFond(prog, changeFond);
       // affCase(lvl.murs, prog);
        affCase(lvl.murs, prog);
        affCase(lvl.cubes, prog);
        affPist(lvl.pist,prog);
        affPerso(lvl.bonhomme, prog);
        affBulle(bul, prog);
        lvl.gravitage(bul);

        for (auto it = bul.begin() ; it !=bul.end() ;){
            if(it->deBulle()){
                bul.erase(it);
            }
            else {
                it++;
            }

        }

        auto grave = std::to_string(-constante::gravity);
        size_t p = 4;
        if(constante::gravity > 0){
            p = 5;
        }
        glish::display(plop, grave.substr(0, p), glm::vec2(sizeLetter, constante::height-sizeLetter), glm::vec2(sizeLetter));


        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        SDL_GL_SwapWindow(win.getWindow());

        if((SDL_GetTicks()- time < 17)) {
            SDL_Delay((Uint32) (17 - (SDL_GetTicks() - time)));
        }

    }
    return 0;
}