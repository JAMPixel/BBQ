//
// Created by cyprien on 08/10/16.
//


#include <iostream>
#include "perso.h"
#include "constante.hpp"
#include <cmath>

Perso::Perso(float posX, float posY) : posX(posX), posY(posY), elan(0), tailleX(15), tailleY(25) {
    std::cout << " je suis en : " << posX << " " << posY << std::endl;
}


void Perso::sauter(std::vector<Bulle> bul) {
    if(constante::gravity==0){}//on peut pas sautey si y'a pas de gravitey !!!!!
    else{

    bool pasBule = true;

    for (auto &b : bul) {
        if (posX > b.x - b.rayon && posX < b.x + b.rayon && posY > b.y - b.rayon && posY < b.y + b.rayon) {

            pasBule = false;
        }
    }
    if(pasBule){
    elan = -3*constante::gravity/std::abs(constante::gravity);}
    else{
        elan = 3*constante::gravity/std::abs(constante::gravity);
    }
}
}

void Perso::deplacerY(std::vector<Bulle> bul) {

    float grav = 0;

    bool pasBule = true;

    for (auto &b : bul) {
        if (posX > b.x - b.rayon && posX < b.x + b.rayon && posY > b.y - b.rayon && posY < b.y + b.rayon) {
            grav += b.gravitance;
            pasBule = false;
        }
    }

    if (pasBule) {
        grav = constante::gravity;
    }
    vitesse.second += grav / 100;
    posY += (vitesse.second + elan);
    if (elan > 0) { elan -= 2; }
    if (elan <= 0) { elan = 0; }
}

void Perso::deplacerX(bool a) {

    if (a) {
        posX += 3;
        vitesse.first = 3;
    } else {
        posX -= 3;
        vitesse.first = -3;
    }
}

void Perso::collision(std::vector<Mobile *> &murs, int &cotecollision, float &distanceinterpenetration) {
    cotecollision = 0;
    distanceinterpenetration = 0;

    for (auto &m:murs) {
        float droite = 0;
        float haut = 0;
        float bas = 0;
        float gauche = 0;
        float Lx;
        float Ly;
        bool collide = false;


        if (posY-tailleY/2  <= m->posY && posY+tailleY/2 >= m->posY + m->tailleY&& posX -std::abs(tailleX)/2  <= m->posX + m->tailleX && posX +std::abs(tailleX)/2 >= m->posX ){
            if(vitesse.first <0){
                posX = m->posX + m->tailleX + std::abs(tailleX)/2;
                vitesse.first = 0;
            }else if (vitesse.first >0){
                posX = m->posX - std::abs(tailleX)/2;
                vitesse.first = 0;
            }


        }else {

            // haut ou droite
            if ((posX + std::abs(tailleX) / 2) > m->posX && (posX + std::abs(tailleX) / 2) < (m->posX + m->tailleX) &&
                posY + tailleY / 2 < m->posY + m->tailleY && posY + tailleY / 2 > m->posY) {
                //droite = posX + tailleX / 2 - m->posX;
                collide = true;
            }
            if (posX - std::abs(tailleX) / 2 > m->posX && (posX - std::abs(tailleX) / 2) < (m->posX + m->tailleX) &&
                posY - tailleY / 2 < m->posY + m->tailleY && posY - tailleY / 2 > m->posY) {
                //gauche = m->posX + m->tailleX - (posX - tailleX / 2);
                collide = true;
            }
            if (posX - std::abs(tailleX) / 2 > m->posX && (posX - std::abs(tailleX) / 2) < (m->posX + m->tailleX) &&
                posY + tailleY / 2 < m->posY + m->tailleY && posY + tailleY / 2 > m->posY) {
                //bas = posY + tailleY / 2 - m->tailleY;
                collide = true;
            }
            if ((posX + std::abs(tailleX) / 2) > m->posX && (posX + std::abs(tailleX) / 2) < (m->posX + m->tailleX) &&
                posY - tailleY / 2 < m->posY + m->tailleY && posY - tailleY / 2 > m->posY) {
                //haut = m->tailleY + m->posY - (posY - tailleY / 2);
                collide = true;
            }

            if (collide) {
                if (posX > m->posX + m->tailleX / 2 && posY < m->posY + m->tailleY / 2) {
                    Lx = m->posX + m->tailleX - (posX - std::abs(tailleX) / 2);
                    Ly = posY + tailleY / 2 - m->posY;
                }
                    // haut ou gauche
                else if (posX < m->posX + m->tailleX / 2 && posY < m->posY + m->tailleY / 2) {
                    Lx = posX + std::abs(tailleX) / 2 - m->posX;
                    Ly = posY + tailleY / 2 - m->posY;
                }
                    // bas ou droite
                else if (posX > m->posX + m->tailleX / 2 && posY > m->posY + m->tailleY / 2) {
                    Lx = m->posX + m->tailleX - (posX - std::abs(tailleX) / 2);
                    Ly = m->posY + tailleY - (posY - tailleY / 2);
                }
                    // bas ou gauche
                else {
                    Lx = posX + std::abs(tailleX) / 2 - m->posX;
                    Ly = m->posY + tailleY - (posY - tailleY / 2);
                }

            }

            /* if ((haut >= bas) && (haut >= gauche) && (haut >= droite) && (haut != 0)) {
                 posY = m->posY + m->tailleY + tailleY / 2;
             }
             if ((bas > haut) && (bas >= gauche) && (bas >= droite) && (bas != 0)) {
                 posY = m->posY - tailleY / 2;
             }
             if ((droite > haut) && (droite > bas) && (droite >= gauche) && (droite != 0)) {
                 posX = m->posX - tailleX / 2;
             }
             if ((gauche >= haut) && (gauche >= bas) && (gauche >= droite) && (gauche != 0)) {
                 posX = m->posX + m->tailleX + tailleX / 2;
             }*/
            if (collide) {
                constexpr float tamere = 0.15;
                constexpr float tonpere = 0.01;

                if ((vitesse.first >= tamere || vitesse.first <= -tamere) &&
                    (vitesse.second >= tamere || vitesse.second <= -tamere)) {
                    float a = std::abs(Lx) / std::abs(vitesse.first);
                    float b = std::abs(Ly) / std::abs(vitesse.second);
                    // on considere que ca marche ish
                    //std::cout << "grr  : "<< a << " "<< b << std::endl;
                    if (a <= b + tonpere && a >= b - tonpere) { std::cout << "coucou"; }
                    if (a > b) {
                        //vers le bas
                        if (vitesse.second > 0) {
                            posY = m->posY - tailleY / 2;
                            //vers le haut
                        } else if (vitesse.second < 0) {
                            posY = m->posY + m->tailleY + tailleY / 2;

                        }
                        vitesse.second = 0;

                    } else {
                        if (vitesse.first > 0) {
                            posX = m->posX - tailleX / 2;
                        } else if (vitesse.first < 0) {
                            posX = m->posX + m->tailleX - tailleX / 2;
                        }
                    }

                    //pas de vitY
                } else if (vitesse.second <= tamere && vitesse.second >= -tamere) {
                    //collision lateral
                    if (posY + tailleY / 2 >= m->posY + tamere && posY - tailleY / 2 <= m->posY + m->tailleY - tamere) {
                        vitesse.second = 0;
                        posX = m->posX - tailleX / 2;
                        if (vitesse.first < 0) {
                            posX += m->tailleX;
                        }
                        // collision vertical
                    } else {
                        if (vitesse.second != 0) {
                            posY = m->posY - (vitesse.second / std::abs(vitesse.second)) * tailleY / 2;
                        } else {
                            posY = m->posY - tailleY / 2;
                        }
                        if (vitesse.second < 0) {
                            posY += m->tailleY;
                        }
                        vitesse.second = 0;
                    }

                    // vitY, pas vitX
                } else if (vitesse.first <= tamere && vitesse.first >= -tamere) {
                    //collision lateral
                    if (!(posX + std::abs(tailleX) / 2 >= m->posX + tamere &&
                          posX - std::abs(tailleX) / 2 <= m->posX + std::abs(m->tailleX) - tamere)) {
                        posX = m->posX - tailleX / 2;
                        if (vitesse.first < 0) {
                            posX += m->tailleX;
                        }
                        // collision vertical
                    } else {
                        if (vitesse.second != 0) {
                            posY = m->posY - (vitesse.second / std::abs(vitesse.second)) * tailleY / 2;
                        } else {
                            posY = m->posY - tailleY / 2;
                        }
                        if (vitesse.second < 0) {
                            posY += m->tailleY;
                        }
                        vitesse.second = 0;

                    }


                }

            }
        }
    }
}
void Perso::collision(std::vector<Piston *> &murs, int &cotecollision, float &distanceinterpenetration) {
    cotecollision = 0;
    distanceinterpenetration = 0;

    for (auto &m:murs) {
        float droite = 0;
        float haut = 0;
        float bas = 0;
        float gauche = 0;
        float Lx;
        float Ly;
        bool collide = false;


        if (posY-tailleY/2  <= m->posY && posY+tailleY/2 >= m->posY + m->tailleY&& posX -std::abs(tailleX)/2  <= m->posX + m->tailleX && posX +std::abs(tailleX)/2 >= m->posX ){
            if(vitesse.first <0){
                posX = m->posX + m->tailleX + std::abs(tailleX)/2;
                vitesse.first = 0;
            }else if (vitesse.first >0){
                posX = m->posX - std::abs(tailleX)/2;
                vitesse.first = 0;
            }


        }else {

            // haut ou droite
            if ((posX + std::abs(tailleX) / 2) > m->posX && (posX + std::abs(tailleX) / 2) < (m->posX + m->tailleX) &&
                posY + tailleY / 2 < m->posY + m->tailleY && posY + tailleY / 2 > m->posY) {
                //droite = posX + tailleX / 2 - m->posX;
                collide = true;
            }
            if (posX - std::abs(tailleX) / 2 > m->posX && (posX - std::abs(tailleX) / 2) < (m->posX + m->tailleX) &&
                posY - tailleY / 2 < m->posY + m->tailleY && posY - tailleY / 2 > m->posY) {
                //gauche = m->posX + m->tailleX - (posX - tailleX / 2);
                collide = true;
            }
            if (posX - std::abs(tailleX) / 2 > m->posX && (posX - std::abs(tailleX) / 2) < (m->posX + m->tailleX) &&
                posY + tailleY / 2 < m->posY + m->tailleY && posY + tailleY / 2 > m->posY) {
                //bas = posY + tailleY / 2 - m->tailleY;
                collide = true;
            }
            if ((posX + std::abs(tailleX) / 2) > m->posX && (posX + std::abs(tailleX) / 2) < (m->posX + m->tailleX) &&
                posY - tailleY / 2 < m->posY + m->tailleY && posY - tailleY / 2 > m->posY) {
                //haut = m->tailleY + m->posY - (posY - tailleY / 2);
                collide = true;
            }

            if (collide) {
                if (posX > m->posX + m->tailleX / 2 && posY < m->posY + m->tailleY / 2) {
                    Lx = m->posX + m->tailleX - (posX - std::abs(tailleX) / 2);
                    Ly = posY + tailleY / 2 - m->posY;
                }
                    // haut ou gauche
                else if (posX < m->posX + m->tailleX / 2 && posY < m->posY + m->tailleY / 2) {
                    Lx = posX + std::abs(tailleX) / 2 - m->posX;
                    Ly = posY + tailleY / 2 - m->posY;
                }
                    // bas ou droite
                else if (posX > m->posX + m->tailleX / 2 && posY > m->posY + m->tailleY / 2) {
                    Lx = m->posX + m->tailleX - (posX - std::abs(tailleX) / 2);
                    Ly = m->posY + tailleY - (posY - tailleY / 2);
                }
                    // bas ou gauche
                else {
                    Lx = posX + std::abs(tailleX) / 2 - m->posX;
                    Ly = m->posY + tailleY - (posY - tailleY / 2);
                }

            }

            /* if ((haut >= bas) && (haut >= gauche) && (haut >= droite) && (haut != 0)) {
                 posY = m->posY + m->tailleY + tailleY / 2;
             }
             if ((bas > haut) && (bas >= gauche) && (bas >= droite) && (bas != 0)) {
                 posY = m->posY - tailleY / 2;
             }
             if ((droite > haut) && (droite > bas) && (droite >= gauche) && (droite != 0)) {
                 posX = m->posX - tailleX / 2;
             }
             if ((gauche >= haut) && (gauche >= bas) && (gauche >= droite) && (gauche != 0)) {
                 posX = m->posX + m->tailleX + tailleX / 2;
             }*/
            if (collide) {
                constexpr float tamere = 0.15;
                constexpr float tonpere = 0.01;

                if ((vitesse.first >= tamere || vitesse.first <= -tamere) &&
                    (vitesse.second >= tamere || vitesse.second <= -tamere)) {
                    float a = std::abs(Lx) / std::abs(vitesse.first);
                    float b = std::abs(Ly) / std::abs(vitesse.second);
                    // on considere que ca marche ish
                    //std::cout << "grr  : "<< a << " "<< b << std::endl;
                    if (a <= b + tonpere && a >= b - tonpere) { std::cout << "coucou"; }
                    if (a > b) {
                        //vers le bas
                        if (vitesse.second > 0) {
                            posY = m->posY - tailleY / 2;
                            //vers le haut
                        } else if (vitesse.second < 0) {
                            posY = m->posY + m->tailleY + tailleY / 2;

                        }
                        vitesse.second = 0;

                    } else {
                        if (vitesse.first > 0) {
                            posX = m->posX - tailleX / 2;
                        } else if (vitesse.first < 0) {
                            posX = m->posX + m->tailleX - tailleX / 2;
                        }
                    }

                    //pas de vitY
                } else if (vitesse.second <= tamere && vitesse.second >= -tamere) {
                    //collision lateral
                    if (posY + tailleY / 2 >= m->posY + tamere && posY - tailleY / 2 <= m->posY + m->tailleY - tamere) {
                        vitesse.second = 0;
                        posX = m->posX - tailleX / 2;
                        if (vitesse.first < 0) {
                            posX += m->tailleX;
                        }
                        // collision vertical
                    } else {
                        if (vitesse.second != 0) {
                            posY = m->posY - (vitesse.second / std::abs(vitesse.second)) * tailleY / 2;
                        } else {
                            posY = m->posY - tailleY / 2;
                        }
                        if (vitesse.second < 0) {
                            posY += m->tailleY;
                        }
                        vitesse.second = 0;
                    }

                    // vitY, pas vitX
                } else if (vitesse.first <= tamere && vitesse.first >= -tamere) {
                    //collision lateral
                    if (!(posX + std::abs(tailleX) / 2 >= m->posX + tamere &&
                          posX - std::abs(tailleX) / 2 <= m->posX + std::abs(m->tailleX) - tamere)) {
                        posX = m->posX - tailleX / 2;
                        if (vitesse.first < 0) {
                            posX += m->tailleX;
                        }
                        // collision vertical
                    } else {
                        if (vitesse.second != 0) {
                            posY = m->posY - (vitesse.second / std::abs(vitesse.second)) * tailleY / 2;
                        } else {
                            posY = m->posY - tailleY / 2;
                        }
                        if (vitesse.second < 0) {
                            posY += m->tailleY;
                        }
                        vitesse.second = 0;

                    }


                }

            }
        }
    }
}
void Perso::modgrav(bool b) {
    constexpr float modif = 0.1;
    constexpr float max = 10.0f;
    if (b && constante::gravity <= max)
        constante::gravity += modif;
    else if (!b && constante::gravity >= -max)
        constante::gravity -= modif;

}











