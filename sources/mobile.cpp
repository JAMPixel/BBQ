//
// Created by cyprien on 08/10/16.
//
#include <glm/vec2.hpp>
#include <cmath>
#include <glm/geometric.hpp>
#include "constante.hpp"
#include "mobile.h"

Mobile::Mobile(float posX, float posY, float tailleX, float tailleY, float force) : posX(posX), posY(posY),
                                                                                    force(force), tailleX(tailleX),
                                                                                    tailleY(tailleY) {}

void Mobile::deplacerY(std::vector<Bulle> bul) {

    float grav = 0;

    bool pasBule = true;

    for (auto &b : bul) {
        if (posX > b.x - b.rayon && posX < b.x + b.rayon && posY > b.y - b.rayon && posY < b.y + b.rayon) {
            grav += b.gravitance;
            pasBule = false;
        }
    }
    if (pasBule) {
        grav = constante::gravity;
    }

    vitesse.second += grav/100;
    posY+=(vitesse.second + force) ;


}

bool Mobile::collision(std::vector<Mobile *> &murs) {
    glm::vec2 dir ;
    bool a= true;
    for (auto &m:murs) {
    float bas=0;
    float haut=0;

        //collision avec bas de cube du decor
        if (posY >= m->posY && posX + tailleX >= m->posX && posX <= m->posX + m->tailleX &&
                posY <= m->posY + m->tailleY ) {
            bas=m->posY + m->tailleY -posY;


        }
            //collision avec haut de cube de decor
        else
        {if (posY<= m->posY + m->tailleY && posX + tailleX >= m->posX && posX <= m->posX + m->tailleX &&
                 posY + tailleY>= m->posY) {
                haut= posY + tailleY - m->posY;}

        }
        if(bas<haut){
            posY = m->posY - m->tailleY ;
            vitesse.second = 0;
            a=false;}
        if(haut<bas){
            posY = m->posY + m->tailleY;
            vitesse.second = 0;
            a=false;

        }

    }
    return a;
}

void Mobile::collisionmobile(std::vector<Mobile*> &cubes) {
    for (auto &m:cubes) {
        float bas=0;
        float haut=0;

        //collision avec bas de cube du decor
        if (posY > m->posY && posX + tailleX > m->posX && posX < m->posX + m->tailleX &&
            posY < m->posY + m->tailleY ) {
            bas=m->posY + m->tailleY -posY;
            posY = m->posY - m->tailleY ;
            vitesse.second = 0;

        }
            //collision avec haut de cube de decor

        if (posY< m->posY + m->tailleY && posX + tailleX > m->posX && posX < m->posX + m->tailleX &&
             posY + tailleY> m->posY) {
                haut= posY - m->posY;
                posY = m->posY + m->tailleY;
                vitesse.second = 0;}




        }
    }

