
//
// Created by hole on 09/10/16.
//

#include <constante.hpp>
#include <iostream>
#include "Piston.hpp"



Piston::Piston(float posX, float posY, float tailleX, float tailleY, float aX, float aY, float lim, int orientation):Mobile(posX,posY,tailleX,tailleY), xAttache(aX), yAttache(aY), limite(lim), orientation(orientation){

    if(orientation == 0){
        force = 0.00f;
    }
    if(orientation == 1){
        force = -0.00f;
    }
}


void Piston::deplacerY(std::vector<Bulle> bul) {

    if (orientation == 2 || orientation == 3)
        return;

    float grav = 0;

    bool pasBule = true;

    for(auto &b : bul){
        if (posX > b.x - b.rayon && posX < b.x + b.rayon && posY > b.y - b.rayon && posY < b.y + b.rayon){
            grav += b.gravitance;
            pasBule = false;
        }
    }
    if (pasBule){
        grav = constante::gravity;
    }




    vitesse.second += grav/100;
    posY+=(vitesse.second + force);
    if (posY > yAttache + limite*tailleY && orientation == 0 ){
        posY = yAttache + limite*tailleY;
        vitesse.second=0;
        //std::cout << "  11   "<< std::endl;
    }
    if( posY < yAttache - limite*tailleY && orientation == 1){
        posY = yAttache - limite*tailleY;
        vitesse.second=0;

        //std::cout << "  12   "<< std::endl;
    }

    if (posY < yAttache  && orientation == 0 ){

        posY = yAttache;
        vitesse.second=0;


    }
    if( posY > yAttache  && orientation == 1){
        posY = yAttache ;
        vitesse.second=0;

        //std::cout << "  14   "<< std::endl;
    }

}