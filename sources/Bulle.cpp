//
// Created by hole on 08/10/16.
//

#include <iostream>
#include "Bulle.hpp"

Bulle::Bulle(float x, float y, float gravitance, float rayon, int time) : x(x), y(y), gravitance(gravitance), rayon(rayon),time(time) {}

void Bulle::incremente() {
    if (rayon < 100) {
        gravitance += 0.07;
        rayon += 1.0;
    }
}
void Bulle::decremente() {
    if (rayon < 100) {
        gravitance -= 0.07;
        rayon += 1.0;
    }
}

bool Bulle::deBulle() {
    if (time > 0){
        time--;
        //std::cout << time;
    }
    else{
        if (rayon > 10){
            rayon -= 1.;
        }else
            return true;
    }
    return false;
}
